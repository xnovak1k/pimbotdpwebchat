# PimBotWebChat

#### Description
**Master thesis 2019 VUT FIT**: Chatbot in an enterprise information system

**Author**: Miroslav Novák

Web application with serves as connector with Microsoft Bot Services. It render webchat which you can use test your bot in web-based environment. Webchat use Direct-line channel to make a connection with server.

#### Build

Simple open project in Visual Studo 2017 and build via default builder.

#### Live demo

You can find live demo here: https://pimbotdpwebchat.azurewebsites.net/
