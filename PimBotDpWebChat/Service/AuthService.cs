// ===============================
// Author: Miroslav Nov�k (xnovak1k@stud.fit.vutbr.cz)
// Create date:
// ===


using System.Collections.Generic;

namespace PimBotDpWebChat.Service
{
    /// <summary>
    /// Authentication service
    /// </summary>
    public class AuthService
    {
        public bool AutheticateUser(string login, string password)
        {
            if (Users.ContainsKey(login) && Users[login] == password)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private readonly Dictionary<string, string> Users = new Dictionary<string, string>
        {
            { "xnovak1k", "1234" },
            { "user", "user" },
            { "test", "test" }
        };
    }
}
