﻿// ===============================
// Author: Miroslav Novák (xnovak1k@stud.fit.vutbr.cz)
// Create date:
// ===


using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PimBotDpWebChat.Models;
using PimBotDpWebChat.Service;

namespace PimBotDpWebChat.Controllers
{
    /// <summary>
    /// Controller for handling request.
    /// </summary>
    public class HomeController : Controller
    {       
        private AuthService _authService = new AuthService();

        [HttpGet]
        public IActionResult Index()
        {
            if (Request.Cookies.ContainsKey("login"))
            {
                ViewData["User"] = Request.Cookies["login"];
                return View("~/Views/Home/Bot.cshtml");
            }
            return View();
        }

        [HttpPost]
        public IActionResult Index(string inputLogin, string inputPassword)
        {
            if (inputLogin == null && inputPassword == null)
            {
                Response.Cookies.Delete("login");
                return View();
            }
            if (_authService.AutheticateUser(inputLogin, inputPassword))
            {
                CookieOptions cookies = new CookieOptions();
                cookies.Expires = DateTime.Now.AddDays(1);
                Response.Cookies.Append("login", inputLogin, cookies);
                ViewData["User"] = inputLogin;
                return View("~/Views/Home/Bot.cshtml");
            }
            else
            {
                ViewData["ErrorLogin"] = "Login or password is't correct";

                return View();
            }
        }

        [HttpPost]
        public IActionResult Logout()
        {
            Response.Cookies.Delete("login");
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

