// ===============================
// Author: Miroslav Nov�k (xnovak1k@stud.fit.vutbr.cz)
// Create date:
// ===

namespace PimBotDpWebChat.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}